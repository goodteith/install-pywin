#!/usr/bin/env python3
# vim:fileencoding=utf-8

import os
import sys

import pywinauto


BASE_DIR = os.path.dirname(sys.executable)
installer = os.path.join(
    BASE_DIR, 'python',
    'pywin32-221.win32-py2.7.exe'
)

app = pywinauto.Application().start(installer)

wizard = app['Setup']

wizard.NextButton.click()
wizard.NextButton.click()
wizard.NextButton.click()
wizard.FinishButton.wait('enabled', timeout=30)
wizard.FinishButton.click()
wizard.wait_not('visible')
